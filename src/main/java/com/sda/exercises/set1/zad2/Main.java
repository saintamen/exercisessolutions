package com.sda.exercises.set1.zad2;

/*
* Napisz aplikację w której zadeklaruj dwie zmienne liczbowe, przypisz do nich dwie dowolne wartości.
* PRZYPISZ DO NICH wartości inicjalne i przemnóż je przez siebie. Wynik działania wypisz na konsoli (System.out.println).

 * */
public class Main {
    public static void main(String[] args) {
        // zadeklaruj dwie zmienne liczbowe
        int zmiennaLiczbowaX = 1; // PRZYPISZ DO NICH wartości inicjalne
        int zmiennaLiczbowaY = 3; // PRZYPISZ DO NICH wartości inicjalne


        // przemnóż je przez siebie
        int wynik = zmiennaLiczbowaX * zmiennaLiczbowaY;

        // Wynik działania wypisz na konsoli (System.out.println)
        System.out.println(wynik);
    }
}
