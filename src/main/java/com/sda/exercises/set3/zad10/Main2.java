package com.sda.exercises.set3.zad10;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int wysokosc = sc.nextInt();

        int ile_spacji = wysokosc - 1;
        int ile_gwiazdek = 1;
        for (int poziom = 0; poziom < wysokosc; poziom++) {
            for (int spacja = 0; spacja < ile_spacji; spacja++) {
                System.out.print(" ");
            }
            for (int gwiazdka = 0; gwiazdka < ile_gwiazdek; gwiazdka++) {
                System.out.print("*");

            }
            for (int spacja = 0; spacja < ile_spacji; spacja++) {
                System.out.print(" ");
            }
            ile_spacji--;
            ile_gwiazdek+=2;
            System.out.println();
        }
    }
}
