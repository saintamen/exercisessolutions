package com.sda.exercises.set1.zad14;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int wartosc = sc.nextInt();

        System.out.println("Bezwzględna: " + Math.abs(wartosc));
        System.out.println("Przeciwna: " + -(wartosc));
        System.out.println("Odwrotna: " + 1.0 / (wartosc));
    }
}
