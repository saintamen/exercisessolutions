package com.sda.exercises.set3.zad13;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę:");
        int granicaTabliczkiMnozenia = scanner.nextInt();

        for (int i = 1; i <= granicaTabliczkiMnozenia; i++) {
            for (int j = 1; j <= granicaTabliczkiMnozenia; j++) {
                System.out.print((i * j) + " ");
            }
            System.out.println();
        }
    }
}
