package com.sda.exercises.set2.zad1;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//        int wartosc = 5;
//
//        System.out.println(wartosc++); // po uzyciu liczby zwieksz jej wartosc
//        System.out.println(++wartosc); // przed uzyciem liczby zwieksz jej wartosc

        int[] tablica = {-3, 9, 2, -10, -3, -4, -1, -5, -10, 9};
        // rozmiar tablicy = 10
        // < 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
        // <= 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
//        for (int i = 0; i < tablica.length; i++) {
//            System.out.print(tablica[i] + " ");
//        }
        System.out.println(Arrays.toString(tablica));
        int minimum = tablica[0];
        int maximum = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (minimum > tablica[i]) {
                minimum = tablica[i];
            }
            if (maximum < tablica[i]) {
                maximum = tablica[i];
            }
        }
        System.out.println(minimum);
        System.out.println(maximum);
        /// srednia
        double suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            suma += tablica[i];
        }
        double srednia = suma / tablica.length;
        System.out.println(srednia);
        //
        int iloscWiekszych = 0;
        int iloscMniejszych = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > srednia) {
                iloscWiekszych++;
            }
            if (tablica[i] < srednia) {
                iloscMniejszych++;
            }
        }
        //tablica.length - 1
        // 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
        //tablica.length
        // 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.print(tablica[i] + " ");
        }
    }
}
