package com.sda.exercises.set3.zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int A = sc.nextInt();
        int B = sc.nextInt();

        int suma = 0;
        // sposób pętlą for
        for (int i = A; i <= B; i++) {
            System.out.println("A: " + A + " B: " + B + " i:" + i);
            suma = suma + i;
        }
        System.out.println("Suma liczb: " + suma);
        ///------------------------- Sposób pętlą while
        int suma2 = 0;
        while (A <= B) {
            suma2 = suma2 + A;
            A++;
        }

        System.out.println("Suma2 liczb: " + suma2);
    }
}
