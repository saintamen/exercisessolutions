package com.sda.exercises.set9.zad11;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String ciagZnakow = scanner.next();

        String odwrocony = "";

        for (int i = ciagZnakow.length()-1; i >=0; i--) {
            char znak = ciagZnakow.charAt(i);

            odwrocony += znak;
        }
        System.out.println(odwrocony);
    }
}
