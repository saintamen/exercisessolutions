package com.sda.exercises.set1.zad13;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int N = sc.nextInt();
        Random generator = new Random();

        for (int i = 0; i < N; i++) {
            System.out.print(generator.nextInt()+ " ");
        }
        System.out.println();

        for (int i = 0; i < N; i++) {
            System.out.print(generator.nextDouble()+ " ");
        }
        System.out.println();

        for (int i = 0; i < N; i++) {
            System.out.print(generator.nextBoolean()+ " ");
        }
        System.out.println();


        int poczatek = sc.nextInt();
        int koniec = sc.nextInt();

        int dlugoscZakresu = koniec - poczatek;
        for (int i = 0; i < N; i++) {
            System.out.print((generator.nextInt(dlugoscZakresu) + poczatek)+ " ");
        }
        System.out.println();

        for (int i = 0; i < N; i++) {
            System.out.print(((Math.random() * dlugoscZakresu) + poczatek) + " ");
        }
    }
}
