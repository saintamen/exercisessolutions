package com.sda.exercises.set12.pack1.moviesDatabase;

import java.time.LocalDate;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {

        MoviesDatabase bazaFilmow = new MoviesDatabase();

        bazaFilmow.dodajFilm(new Movie("a", "b", MovieType.ACTION, LocalDate.now()));

        Optional<Movie> film = bazaFilmow.wyszukajFilm("avatar");

        if (film.isPresent()) {
            // wykonanie get() na pustym optionalu zwróci NoSuchElementException
            System.out.println("Data wydania: " + film.get().getDataWydania());
        } else {
            System.out.println("Nie znaleziono filmu!");
        }

        bazaFilmow.wypiszWszystkieFilmy();


    }
}
