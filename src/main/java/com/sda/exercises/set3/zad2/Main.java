package com.sda.exercises.set3.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj ilosc powtorzen:");
        int iloscPowtorzen = sc.nextInt();
        for (int indeks = 0; indeks < iloscPowtorzen; indeks++) {
            System.out.println("hello world.");
        }

        // pętlą while
        int iloscPowtorzenWhile = iloscPowtorzen;
        while (iloscPowtorzenWhile-- > 0) {
            System.out.println("hello world.");
        }
    }
}
