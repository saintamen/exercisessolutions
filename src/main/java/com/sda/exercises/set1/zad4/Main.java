package com.sda.exercises.set1.zad4;

/**
 * Zadeklaruj 3 zmienne - zmienna 'a', zmienna 'b', zmienna 'c'.
 * Przypisz jej 3 wartości - ważne żeby były różne.
 * <p>
 * Następnie wykonaj na nich następujące działania:
 * a) przepisz wartości - do zmiennej 'a' przypisz wartość 'b', do zmiennej 'b' przypisz wartość 'c', do zmiennej 'c' przypisz wartość 'a'.
 * b) wypisz wartości na ekran.
 */
public class Main {
    public static void main(String[] args) {
        // Zadeklaruj 3 zmienne - zmienna 'a', zmienna 'b', zmienna 'c'.
        // Przypisz jej 3 wartości - ważne żeby były różne.
        int a = 3;
        int b = 4;
        int c = 5;


        // Źle !
        // przy tym rozwiązaniu tracimy wartość zmiennej 'a' nadpisując ją w 1 działaniu
//        a = b;
//        b = c;
//        c = a;

        // Dobrze!
        int zmiennaTymczasowa = a;
        a = b;
        b = c;
        c = zmiennaTymczasowa;

        // wypisz wartości na ekran.
        System.out.println("Wartość a: " + a);
        System.out.println("Wartość b: " + b);
        System.out.println("Wartość c: " + c);
    }
}
