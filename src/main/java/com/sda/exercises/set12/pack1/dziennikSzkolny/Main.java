package com.sda.exercises.set12.pack1.dziennikSzkolny;

public class Main {
    public static void main(String[] args) {
        DziennikSzkolny dziennikSzkolny = new DziennikSzkolny();

        dziennikSzkolny.dodajStudenta("1234", "Paweł", "Gaweł");
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.MATEMATYKA, 3.0);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.MATEMATYKA, 3.1);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.MATEMATYKA, 3.2);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.MATEMATYKA, 3.3);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.MATEMATYKA, 3.4);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.INFORMATYKA, 3.5);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.INFORMATYKA, 3.2);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.INFORMATYKA, 3.3);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.INFORMATYKA, 3.4);
        dziennikSzkolny.dodajOcenęDlaStudenta("1234", Przedmiot.INFORMATYKA, 3.5);


        dziennikSzkolny.wypiszOceny("1234");
        dziennikSzkolny.wypiszSrednia("1234");
    }
}
