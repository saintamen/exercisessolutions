package com.sda.exercises.set3.zad1;

public class Main {
    public static void main(String[] args) {

        // przyład H)
        for (int i = 97; i <= 122; i++) {
            System.out.println((char) i);
        }

        // przyład H)
        for (char i = 'a'; i <= 'z'; i++) {
            System.out.println(i);
        }

        // przyład I)
        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.println(i);
        }

        // przyład J)
        for (char i = 'A'; i <= 'Z'; i += 2) {
            System.out.println(i);
        }

        // przyład K)
        for (char i = 'b'; i <= 'z'; i += 2) {
            if (i % 5 == 0) {
                System.out.println(i);
            }
        }

        // przyład L)
        for (int i = 0; i < 100; i++) {
            System.out.println(i + ". Hello world!");
        }
    }
}
