package com.sda.exercises.set1.zad5;

/**
 * Zadeklaruj zmienne logiczne (boolean) które noszą nazwe:
 jest_cieplo
 wieje_wiatr
 swieci_slonce

 oraz zmienne:
 ubieram_sie_cieplo - jesli nie jest cieplo lub wieje wiatr
 biore_parasol - jesli nie swieci slonce ale nie wieje wiatr
 ubieram_kurtke - jesli wieje, nie ma slonca i nie jest cieplo
 */
public class Main {
    public static void main(String[] args) {
        // stworzyłem 3 zmienne i przypisałem do nich jakieś wartości
        boolean jest_cieplo = false;
        boolean wieje_wiatr = false;
        boolean swieci_slonce = true;

        // wynikiem poszczególnych 'łamigłówek' logicznych jest sprawdzenie warunków założeń które pojawiły się wyżej
        boolean ubieram_sie_cieplo = !jest_cieplo || wieje_wiatr ;                      //jesli nie jest cieplo lub wieje wiatr
        boolean biore_parasol = !swieci_slonce && !wieje_wiatr;                         // jesli nie swieci slonce ale nie wieje wiatr


        boolean ubieram_kurtke = wieje_wiatr && !swieci_slonce && !jest_cieplo;         //jesli wieje, nie ma slonca i nie jest cieplo
        System.out.println("Biore parasol: "+ ubieram_sie_cieplo + "Ubieram kurtke: " + ubieram_kurtke + "Ubieram sie cieplo: "+ ubieram_sie_cieplo);
    }
}
