package com.sda.exercises.set3.zad6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe:");
        int liczba = sc.nextInt();


        // sposób 1
        int sprawdzana = 1;
        while (sprawdzana < liczba) {
            System.out.println(sprawdzana);
            sprawdzana *= 2;
        }

        // sposób 2
        sprawdzana = 0;
        while (Math.pow(2, sprawdzana) < liczba) {
            System.out.println(Math.pow(2, sprawdzana));
            sprawdzana += 1;
        }
    }
}
