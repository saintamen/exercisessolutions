package com.sda.exercises.set12.pack1.moviesDatabase;

import java.util.*;
import java.util.stream.Collectors;

public class MoviesDatabase {
    private Map<String, Movie> filmy = new HashMap<>();

    public MoviesDatabase() {
    }

    public void dodajFilm(Movie filmDoDodania) {
        filmy.put(filmDoDodania.getTytul(), filmDoDodania);
    }

    public Optional<Movie> wyszukajFilm(String nazwaFilmu) {
        Movie film = filmy.get(nazwaFilmu);
        // jeśli zastosujemy Optional.of, to rzuci nam NullPointerException jeśli nie znajdzie elementu
        return Optional.ofNullable(film);
    }

    public void wypiszWszystkieFilmy() {
        for (Movie film : filmy.values()) {
            System.out.println(film);
        }
    }

    public List<Movie> znajdzFilmyTypu(MovieType typ) {
//        List<Movie> filmy = new ArrayList<>();
//
//        for (Movie film : filmy.values()) {
//            if(film.getType() == typ){
//                filmy.add(film);
//            }
//        }
//        return filmy;
        return filmy.values().stream()
                .filter(film -> film.getType() == typ)
                .collect(Collectors.toList());
    }
}
