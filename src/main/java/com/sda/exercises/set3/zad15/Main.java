package com.sda.exercises.set3.zad15;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj znaki:");
        String slowo = sc.next();

        int dlugoscSlowa = slowo.length();

        boolean czyJestPalindromem = true;

        for (int i = 0; i < (slowo.length()/2); i++) {
            char znakOdKonca = slowo.charAt(dlugoscSlowa - 1 - i);
            char znakOdPoczatku = slowo.charAt(i);

            System.out.println("Porownuje od konca: '" + znakOdKonca + "' z od poczatku: '" + znakOdPoczatku + "'");

            if(znakOdKonca != znakOdPoczatku){
                czyJestPalindromem = false;
//                break;
            }
        }

        if(!czyJestPalindromem ){
            System.out.println("Nie jest palindromem");
        }else{
            System.out.println("Jest palindromem");
        }
    }
}
