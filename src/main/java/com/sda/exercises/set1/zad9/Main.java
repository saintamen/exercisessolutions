package com.sda.exercises.set1.zad9;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Napisać program służący do konwersji wartości temperatury podanej w
        // stopniach Celsjusza na stopnie w skali Fahrenheita (stopnie Fahrenheita = 1.8 * stopnie Celsjusza + 32.0)
        Scanner sc = new Scanner(System.in);

        double temp = sc.nextDouble();
        System.out.println("Temp w Fahrenheita = " + (temp * 1.8 + 32));
    }
}
