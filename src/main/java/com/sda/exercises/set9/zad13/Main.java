package com.sda.exercises.set9.zad13;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String wyrazenie = sc.nextLine();

        // )4*2(

        int iloscOtwartych = 0;
        int iloscDomknietych = 0;

        int licznik = 0;

        for (int i = 0; i < wyrazenie.length(); i++) {
            char znak = wyrazenie.charAt(i);

            if (znak == '(') {
                iloscOtwartych++;
                licznik++;
            }
            if (znak == ')') {
                iloscDomknietych++;
                licznik--;
            }

            if (licznik < 0) {
                break;
            }
        }

        if (iloscDomknietych == iloscOtwartych && licznik == 0) {
            System.out.println("Wyrazenie jest ok.");
        }else{
            System.out.println("Niepoprawne równanie");
        }
    }
}
