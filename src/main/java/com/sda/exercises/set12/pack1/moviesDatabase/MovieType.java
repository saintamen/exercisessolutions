package com.sda.exercises.set12.pack1.moviesDatabase;

public enum MovieType {
    ACTION, DRAMA, COMEDY, HORROR
}
