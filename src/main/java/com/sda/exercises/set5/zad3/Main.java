package com.sda.exercises.set5.zad3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double liczba1 = sc.nextDouble();
        double liczba2 = sc.nextDouble();

        try {
            if (liczba2 == 0.0) {
                throw new NumberFormatException("Cholero dzielisz przez zero");
            }


            System.out.println(liczba1 / liczba2);

        } catch (NumberFormatException exception_jakis) {
            System.out.println("Komunikat " + exception_jakis.getMessage());
            exception_jakis.printStackTrace();
        }

    }
}
