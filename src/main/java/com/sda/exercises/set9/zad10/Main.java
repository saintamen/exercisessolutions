package com.sda.exercises.set9.zad10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj słowo:");
        String slowoOdUzytkownika = scanner.next();

        int dlugoscSlowa = slowoOdUzytkownika.length();
        int indeksOstatniejLitery = dlugoscSlowa - 1;
        char literaNaOstatniejPozycji = slowoOdUzytkownika.charAt(indeksOstatniejLitery);


        // kod ascii
        int kodAscii = literaNaOstatniejPozycji;

        int licznik = 0;
        for (int i = 0; i < dlugoscSlowa; i++) {
            if (slowoOdUzytkownika.charAt(i) == literaNaOstatniejPozycji) {
                licznik++;
            }
        }

        System.out.println("Ilosc wystapien: " + licznik);

    }
}
