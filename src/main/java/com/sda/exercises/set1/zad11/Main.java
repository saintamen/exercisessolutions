package com.sda.exercises.set1.zad11;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj kwotę dochodu:");
        double dochod = sc.nextDouble();
        if (dochod < 85528) {
            double podatek = dochod * .18 - 556.02;
            System.out.println("Podatek wynosi: " + podatek);
        } else {
            double podatek = 14839.02;
            podatek = podatek + ((dochod - 85528) * 0.32);
            System.out.println("Podatek wynosi: " + podatek);
        }

    }
}
